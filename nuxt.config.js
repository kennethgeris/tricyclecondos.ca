module.exports = {
  /*
  ** Headers of the page
  */
 modules:[
  ['@nuxtjs/bootstrap-vue', { css: true }],
],
plugins: [
  { src:'~plugins/ga', ssr: false },
  ],
  head: {
    title: 'Tricycle - Register Now',
    script:[
      {src:'https://js.createsend1.com/javascript/copypastesubscribeformlogic.js'},
      {src:'https://code.jquery.com/jquery-3.3.1.min.js'}

    ],
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Tricycle - Register Now' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {rel: 'stylesheet', href: 'https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css'}
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#D8262F' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */   
   
  extractCSS: true,
   vendor: [
    'babel-polyfill'
   ],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

